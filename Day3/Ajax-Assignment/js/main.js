$(document).ready(function()
{
    $('#users').DataTable( {
        serverSide: true,
        ajax: {
            url: 'http://jsonplaceholder.typicode.com/users',
            type: 'GET',
            dataSrc: ''
        },
        columns: [
            {data: 'id'},
            {data: 'name'},
            {data: 'username'},
            {data: 'email'},
            {data: 'address.city'},
            {data: 'phone'},
            {data: 'website'},
            {data: 'company.name'}
        ]
    } );

    /*
    $.ajax({
        url: "http://jsonplaceholder.typicode.com/users", 
        type: "GET",    
        dataType:"json",   
        success: function (response) 
        {
          for(var user in response){
              let id = response[user]['id'];
              let name = response[user]['name'];
              let username = response[user]['username'];
              let email = response[user]['email'];
              let city = response[user]['address']['city'];
              let street = response[user]['address']['street'];
              let phone = response[user]['phone'];
              let website = response[user]['website'];
              let company = response[user]['company']['name'];

              let tableRow = $('<tr></tr>');

              tableRow
              .append($(`<td>${id}</td>`))
              .append($(`<td>${name}</td>`))
              .append($(`<td>${username}</td>`))
              .append($(`<td>${email}</td>`))
              .append($(`<td>${street}, ${city}</td>`))
              .append($(`<td>${phone}</td>`))
              .append($(`<td>${website}</td>`))
              .append($(`<td>${company}</td>`));

              $('#users').append($(tableRow));
          }
        }   
    });
    */
});
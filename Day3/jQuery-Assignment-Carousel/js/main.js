$(document).ready(main);

var slideIndex = 1;
showCarousel(slideIndex);

function changePic(n) {
    showCarousel(slideIndex += n);
}

function showCarousel(n) {
    var images = $('.image');
    if (n > images.length) {
        slideIndex = 1;
    }

    if (n < 1) {
        slideIndex = images.length;
    }

    for (i = 1; i < images.length; i++) {
        $(images[i]).hide();
    }
    $(images[slideIndex - 1]).show();
}

function main() {
    $('.previous-button').on('click', function(){
        changePic(-1);
    })
    $('.next-button').on('click', function(){
        changePic(+1);
    })
}
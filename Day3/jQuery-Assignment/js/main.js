function main() {
    $('.hide-button').on('click', function () {
        $('.image-bar').hide();
    });

    $('.show-button').on('click', function () {
        $('.image-bar').show();
    });

    $('.round-button').on('click', function () {
        $('.image').css('border-radius', '25px');
    });

    $('.straight-button').on('click', function () {
        $('.image').css('border-radius', '0px');
    });

    $('.add-red-border-button').on('click', function () {
        $('.image').addClass('change-border');
    });

    $('.remove-red-border-button').on('click', function () {
        $('.image').removeClass('change-border');
    });

    $('.up-button').on('click', function () {
        $('.image').css('top', '-10px');
    });

    $('.down-button').on('click', function () {
        $('.image').css('top', '10px');
    });
}

$(document).ready(main);